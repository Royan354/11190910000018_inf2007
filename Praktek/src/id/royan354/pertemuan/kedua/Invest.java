/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kedua;

/**
 *
 * @author 62813
 */
public class Invest {

    public static void main(String[] arguments) {
        float total = 1400;
        System.out.println("Original investment: $" + total);

        //inceases by 40 percent the first year
        total = total + (total * .4F);
        System.out.println("after one year: $" + total);
        
        //loses $1,500 the second year
        total = total - 1500F;
        System.out.println("after one years: $" + total);
        
        //Increases by 12 percent the third year
        total = total + (total * .12F);
        System.out.println("after three years: $" + total);
    }

}
