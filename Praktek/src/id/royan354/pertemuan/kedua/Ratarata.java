/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kedua;
import java.util.Scanner;
/**
 *
 * @author 62813
 */
public class Ratarata {
    
    public static void main(String[] args)  {
        int p, q, r;
        
        Scanner in = new Scanner(System.in);
        p = in.nextInt();//suhu minimal 
        q = in.nextInt();//suhu maximal
        r = (p + q)/2;
        
        System.out.println("Suhu minimal suatu hari tertentu adalah " + p);
        System.out.println("Suhu maximal suatu hari tertentu adalah " + q);
        System.out.println("Suhu rata-rata suatu hari tertentu adalah " + r);
    }
}
