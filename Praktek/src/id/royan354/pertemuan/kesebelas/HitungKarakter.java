/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.io.IOException;
import java.util.logging.Logger;
/**
 *
 * @author 62813
 */
public class HitungKarakter {
    int getHitungKarakter(FileReader T) {
        int n;
        char[] c;
        n = 0;
        Scanner line = new Scanner (new BufferedReader(T));
        while (line.hasNext()){
            c = line.next().toCharArray();
            for(char d : c) {
                if(d == 'a'){
                    n = n + 1;
                }
            }
        }
        return n;
    }
    public static void main(String[] args) {
        HitungKarakter hitung = new HitungKarakter();
        int a;
        try{
            a = hitung.getHitungKarakter(new FileReader("D:\\Ojan\\ContohFile.txt"));
            System.out.println("Jumlah Huruf a = " + a);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(HitungKarakter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
