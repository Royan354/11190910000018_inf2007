package id.royan354.pertemuan.kesebelas;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class ArsipBilanganBulat {

    public static void main(String[] args) {
        File file = new File("D:\\Ojan\\File1.txt");
        int n, i;
        Scanner in = new Scanner(System.in);
        try {
            PrintWriter output = new PrintWriter(file);
            System.out.println("Masukan Nilai : ");
            n = in.nextInt();
            for (i = 0; i < n; i++) {
                output.println(i);
            }
            output.close();
        } catch (IOException ex) {
            System.out.printf("ERROR : %s/n", ex);
        }
    }
}
