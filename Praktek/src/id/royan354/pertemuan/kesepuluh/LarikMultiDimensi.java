package id.royan354.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class LarikMultiDimensi {
    int[][] getLariMultiDimensi(int[][] L, int Nbar, int Nkol) {
        int i, j, temp, tambah = 0;
        int ntotal = Nbar * Nkol;
        int[] Array = new int[ntotal];
        
        for (i = 0; i < Nbar; i++) {
            for (j = 0; j < Nkol; j++) {
                Array[tambah] = L[i][j];
                tambah++;
            }
        }
        System.out.println("Nilai-nilai Larik Penampung");
        System.out.println(Arrays.toString(Array));
        
        for (i = 0; i < ntotal - 1; i++) {
            for (j = ntotal - 1; j > i; j--) {
                if (Array[j] < Array[j - 1]){
                    temp = Array[j];
                    Array[j] = Array[j - 1];
                    Array[j - 1] = temp;
                }
            }
        }
        System.out.println("Nilai-nilai Larik Penampung yang terurut");
        System.out.println(Arrays.toString(Array));
        
        tambah = 0;
        for (i = 0; i < Nbar; i++) {
            for ( j = 0; j < Nkol; j++) {
                L[i][j] = Array[tambah];
                tambah++;
            }
        }
        return L;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        LarikMultiDimensi s = new LarikMultiDimensi();
        int nbar, nkol;
        int i, j;
        System.out.print("Masukan Jumlah barik : ");
        nbar = in.nextInt();
        System.out.print("Masukan jumlah kolom : ");
        nkol = in.nextInt();
        int [][] L = new int[nbar][nkol];
        for (i = 0; i < nbar; i++) {
            System.out.println("Masukan nilai larik baris ke " + (i + 1));
            for (j = 0; j < nkol; j++) {
                L[i][j] = in.nextInt();
            }
        }
        System.out.println("Larik yang belom diurutkan");
        System.out.println(Arrays.deepToString(L));
        s.getLariMultiDimensi(L, nbar, nkol);
        System.out.println("Larik yang sudah diurutkan");
        System.out.println(Arrays.deepToString(L));
    }
}
