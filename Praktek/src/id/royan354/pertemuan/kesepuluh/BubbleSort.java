package id.royan354.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author 62813
 */
public class BubbleSort {
    public int[] getBubbleSort (int L[], int n) {
        int temp;
        for(int i = 0; i < n - 1; i++) {
            for (int k = n - 1; k > i; k--) {
                System.out.println("i : " + i + ", k : " + (k + 1) + " --> " + L[k - 1]);
                if (L[k] < L[k - 1]) {
                    temp = L[k];
                    L[k] = L[k - 1];
                    L[k - 1] = temp;
                }
            }
        }
        return L;
    }
    public static void main(String[] args) {
        int L[] = {29, 27, 10, 8, 76, 21};
        int n = L.length;
        BubbleSort app = new BubbleSort();
        System.out.println("Bilangan Belum Terurut : ");
        System.out.println(Arrays.toString(L));
        app.getBubbleSort(L, n);
        System.out.println("Bilangan Setelah Terurut : ");
        System.out.println(Arrays.toString(L));
    }
}
