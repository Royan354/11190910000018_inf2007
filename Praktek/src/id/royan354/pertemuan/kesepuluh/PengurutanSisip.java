package id.royan354.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class PengurutanSisip {
    int[] getPengurutanSisip(int[] L, int N) {
        Scanner in = new Scanner(System.in);
        int i, j, k, y;
        boolean ketemu;
        System.out.println("Masukan Nilai Larik");
        for (k = N - 1; k >= 0; k--) {
            L[k] = in.nextInt();
            for (i = 1; i < N; i++) {
                y = L[i];
                j = i - 1;
                ketemu = false;
                while ((j >= 0) && (!ketemu)) {
                    if (y > L[j]) {
                        L[j + 1] = L[j];
                        j = j - 1;
                    }else {
                        ketemu = true;
                    }
                }
                L[j + 1] = y;
            }
            System.out.println(Arrays.toString(L));
        }
        return L;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        PengurutanSisip sort= new PengurutanSisip();
        int N;
        System.out.println("Masukan Panjang Larik");
        N = in.nextInt();
        int[] L = new int[N];
        sort.getPengurutanSisip(L, N);
    }
}
