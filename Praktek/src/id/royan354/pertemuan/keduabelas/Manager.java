package id.royan354.pertemuan.keduabelas;

/**
 *
 * @author 62813
 */
public class Manager extends Pegawai {
    private int Tunjangan;
    
    public Manager (String Nama, int gaji, int Tunjangan) {
        super(Nama,gaji);
        this.Tunjangan = Tunjangan;
    }
    
    public int InfoGaji() {
        return this.gaji;
    }
    public int InfoTunjangan() {
        return this.Tunjangan;
    }
}
