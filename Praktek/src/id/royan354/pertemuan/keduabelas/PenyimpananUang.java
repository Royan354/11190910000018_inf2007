package id.royan354.pertemuan.keduabelas;

/**
 *
 * @author 62813
 */
public class PenyimpananUang extends Tabungan {  
    private double TingkatBunga;
    
    public PenyimpananUang(int Saldo, double TingkatBunga) {
        super(Saldo);
        this.TingkatBunga = TingkatBunga;
    }
    
    public double CekUang() {
        this.Saldo = (int) (Saldo + (Saldo * TingkatBunga));
        return this.Saldo;
    }
}
