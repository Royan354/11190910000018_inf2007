package id.royan354.pertemuan.keduabelas;

/**
 *
 * @author 62813
 */
public class Bayaran {
    public int HitungBayaran (Pegawai pegawai) {
        int Uang = pegawai.InfoGaji();
        if (pegawai instanceof Manager) {
            Uang += ((Manager)pegawai).InfoTunjangan();
        } else if (pegawai instanceof Programmer) {
            Uang += ((Programmer)pegawai).InfoBonus();
        }
        return Uang;
    }
    public static void main(String[] args) {
        Manager m = new Manager("Budi", 800, 50);
        Programmer p = new Programmer("Cecep", 600, 30);
        Bayaran b = new Bayaran();
        System.out.println("Upah Manager : " + b.HitungBayaran(m));
        System.out.println("Upah Programmer : " + b.HitungBayaran(p));
    }
}
