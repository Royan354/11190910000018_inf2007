package id.royan354.pertemuan.keduabelas;

/**
 *
 * @author 62813
 */
public class Pegawai {
    protected String Nama;
    protected int gaji;
    
    public Pegawai(String Nama, int gaji) {
        this.Nama = Nama;
        this.gaji = gaji;
    }
    
    public int InfoGaji() {
        return this.gaji;
    }
}
