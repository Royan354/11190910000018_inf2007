package id.royan354.pertemuan.keduabelas;

/**
 *
 * @author 62813
 */
public class PenyimpananUangTest {
    public static void main(String[] args) {
        PenyimpananUang tabungan = new PenyimpananUang(5000, 8.5/100);
        System.out.println("Uang yang ditabung : 5000");
        System.out.println("Tingkat Bunga Sekarang : 8.5%");
        System.out.println("Total Uang Anda Sekarang : " + tabungan.Saldo);
    }
}
