package id.royan354.pertemuan.keduabelas;

/**
 *
 * @author 62813
 */
public class TabunganTest {
    public static void main(String[] args) {
        Tabungan t = new Tabungan(5000);
        System.out.println("Saldo Awal = " + t.Saldo);
        t.AmbilUang(2300);
        System.out.println("Jumlah Uang yang diambil = 2300");
        System.out.println("Saldo Sekarang = " + t.Saldo);
    }
}
