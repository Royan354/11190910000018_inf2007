package id.royan354.pertemuan.keduabelas;

/**
 *
 * @author 62813
 */
public class Programmer extends Pegawai{
    private int Bonus;
    private int gaji;
    
    public Programmer(String Nama, int gaji, int Bonus) {
        super(Nama,gaji);
        this.Bonus = Bonus;
    }
    
    public int InfoGaji() {
        return this.gaji;
    }
    
    public int InfoBonus() {
        return this.Bonus;
    }
}
