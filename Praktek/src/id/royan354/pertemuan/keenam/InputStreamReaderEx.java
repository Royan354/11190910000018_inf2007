/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.keenam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author 62813
 */
public class InputStreamReaderEx {
    public static void main(String[] args) {
        int bilangan;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.println("Masukan bilangan : ");
        try {
            bilangan = Integer.parseInt(in.readLine());
        } catch (IOException ex)  {
            System.out.println("error : " + ex.toString());
        }
    }
}
