/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.keenam;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class HeadlingThrow {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        try {
            System.out.println("Masukan Angka : ");
            int num = in.nextInt();
            if (num > 10 ) throw new Exception();
            System.out.println("Angka Kurang Dari Atau Sama Dengan 10");
        } catch (Exception err) {
            System.out.println("Angka Lebih Dari 10");
        }
        System.out.println("Selesai");
    }
}
