/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class ArrayMinimum {
    private static int getMin(int[] A, int N) {
        int i, min;
        min = 9999;
        for (i = 0; i < N; i++) {
            if (A[i] < min) {
                min = A[i];
            }
        }
        return min;
    }
    public static void main(String[] args) {
        int i, N;
        Scanner in = new Scanner(System.in);
        System.out.println("Jumlah Elemen Larik");
        N = in.nextInt();
        int [] A = new int [N];
        System.out.println("Masukan Nilai-Nilai Larik");
        for (i = 0; i < N; i++) {
            A[i] = in.nextInt();
        }
        System.out.println("Elemen Terkecil = " + getMin(A, N));
    }
}
