/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class ArrayMaksimum {
    private static int getMaks(int[] A, int n) {
        int i, maks;
        maks = -9999;
        for (i = 1;i <= n; i++) {
            if (A[i] > maks) {
                maks = A[i];
            }
        }return maks;
    }
    public static void main(String[] args) {
        int[] A = new int [100];
        int i, n;
        Scanner in = new Scanner(System.in);
        System.out.println("Ukuran Efektif Larik");
        n = in.nextInt();
        System.out.println("Input Nilai-nilai Larik");
        for (i = 1; i <= n; i++) {
            A[i] = in.nextInt();
        }
        System.out.println("Elemen Terbesar = " + getMaks(A,n));
    }
}

