/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class AplikasiMatriks {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Matriks matriks = new Matriks();
        int Nbar, Nkol;
        int i, j;
        System.out.println("Masukan Jumalh Baris");
        Nbar = in.nextInt();
        System.out.println("Masukan Jumlah Kolom");
        Nkol = in.nextInt();
        int[][] Array1 = new int[Nbar][Nkol];
        int[][] Array2 = new int[Nbar][Nkol];
        
        for (i = 0; i < Nbar; i++) {
            System.out.println("Masukan Nilai Larik 1 Baris ke " + (i + 1));
            for (j = 0; j < Nkol; j++) {
                Array1[i][j] = in.nextInt();
            }
        }
        for (i = 0; i < Nbar; i++) {
            System.out.println("Masukan Nilai Larik 2 Baris ke " + (i + 1));
            for (j = 0; j < Nkol; j++) {
                Array2[i][j] = in.nextInt();
            }
        }
        System.out.println("Matriks Ke 1");
        for (i = 0; i < Nbar; i++) {
            System.out.print("[");
            for (j = 0; j < Nkol; j++) {
                System.out.print(" " + Array1[i][j] + "");
            }
            System.out.println("]");
        }
        System.out.println("Matriks Ke 2");
        for (i = 0; i < Nbar; i++) {
            System.out.print("[");
            for (j = 0; j < Nkol; j++) {
                System.out.print(" " + Array2[i][j] + "");
            }
            System.out.println("]");
        }
        System.out.println("Matriks Hasil Perjumlahan");
        matriks.getPenambahanMatriks(Array1, Array2, Nbar, Nkol);
    }
}
