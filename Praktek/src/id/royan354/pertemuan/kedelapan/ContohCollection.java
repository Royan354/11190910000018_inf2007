/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kedelapan;

import java.util.*;

/**
 *
 * @author 62813
 */
public class ContohCollection {
    public static void main(String[] args) {
        System.out.println("--- List ---");
        List NamaList = new ArrayList();
        NamaList.add(1);
        NamaList.add(1);
        NamaList.add(2);
        for (int i = 0; i < NamaList.size(); i++) {
            System.out.println(NamaList.get(i));   
        }
        System.out.println("---Set---");
        Set namaset = new HashSet();
        namaset.add(1);
        namaset.add(1);
        namaset.add(2);
        for (Object o : namaset) {
            System.out.println(o.toString());
        }
        System.out.println("--- Map ---");
        Map namaMap = new HashMap();
        namaMap.put(0, "1");
        namaMap.put(1, "2");
        namaMap.put(2, "3");
        namaMap.forEach((k,v) -> {System.out.println("key : " + k + "Value : " + v);});
    }
} 

