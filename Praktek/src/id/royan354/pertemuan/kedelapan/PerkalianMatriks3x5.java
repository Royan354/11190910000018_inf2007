/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class PerkalianMatriks3x5 {
    private static int Cetak(int[][] Array) {
        for (int i = 0; i < 3; i++) {
            System.out.print("[");
            for (int j = 0; j < 5; j++) {
                System.out.print(" " + Array[i][j] + " ");
            }
            System.out.println("]");
        }
        return 0;
    }
    private static int Kali(int[][] Array) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++){
                Array[i][j] = Array[i][j] * 3;
            }
        }
        return 0;
    }
    public static void main(String[] args) {
        int[][] Array = new int[3][5];
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Masukan Nilai Larik Barik Ke " + (i + 1));
            for (int j = 0; j < 5; j++) {
                Array[i][j] = in.nextInt();
            }
        }
        System.out.println("Matriks 3x5");
        Cetak(Array);
        Kali(Array);
        System.out.println("Matriks 3x5 Setelah Dikali3");
        Cetak(Array);
    }
}

 