/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kedelapan;

/**
 *
 * @author 62813
 */
public class Matriks {
    public static int [][] getPenambahanMatriks(int[][] A, int[][] B, int Nbar, int Nkol) {
        int i, j;
        int[][] C = new int[Nbar][Nkol];
        for (i = 0; i < Nbar; i++) {
            for (j = 0; j < Nkol; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }
        for (i = 0; i < Nbar; i++) {
            System.out.print("[");
            for (j = 0; j < Nkol; j++) {
                System.out.print(" " + C[i][j] + " ");
            }
            System.out.println("]");
        }return C;
    }
}
