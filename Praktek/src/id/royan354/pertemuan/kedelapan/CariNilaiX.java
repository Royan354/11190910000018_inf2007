/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class CariNilaiX {
    static int Cari(int[] Array, int NilaiX) {
        int i;
        for (i = 0; i < Array.length; i++) {
            if (Array[i] == NilaiX) {
                return i + 1;
            }
        }
        return 0;
    }
    public static void main(String[] args) {
        int i, n, NilaiX, Hasil;
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        int [] Array = new int[n];
        
        System.out.println("Input Nilai-nilai Larik");
        for (i = 0; i < n; i++) {
            Array[i] = in.nextInt();
        }
        System.out.println("Masukan Nilai X");
        NilaiX = in.nextInt();
        
        Hasil = Cari(Array, NilaiX);
        if (Hasil == 0) {
            System.out.println("Indeks 0");
        }else {
            System.out.println("X Urutan Ke " + Cari(Array, NilaiX) + " Dari Larik");
        }
    }
}
