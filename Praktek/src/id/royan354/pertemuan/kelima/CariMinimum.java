package id.royan354.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class CariMinimum {
    public static void main(String[] args) {
        int n, x, min, i;
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        x = in.nextInt();
        min = x;
        for (i = 2; i <= n; i++) {
            x = in.nextInt();
            if (x < min)  {
                min = x;
            }
        }System.out.println(min);
    }
}
