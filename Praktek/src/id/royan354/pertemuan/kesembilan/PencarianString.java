/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class PencarianString {
    int SequentialSearch(String Array[], int n, String x) {
        int i = 0;
        while ((i < n - 1) && (!x.equals(Array[i]))) {
            i = i + 1;
        }if (x.equals(Array[i])) {
            return i;
        }else {
            return -1;
        }
    }
    int BinarySearch(String[] Array, int n, String x) {
        int i = 0, j = n - 1, k = 0;
        boolean ketemu = false;
        while ((i <= j) && (!ketemu)) {
            k = (i + j) / 2;
            if (x.equals(Array[k])) {
                ketemu = true;
            }else {
                if (x.compareTo(Array[k]) > 0) {
                    i = k + 1;
                }else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;
        }else {
            return -1;
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String A [] = {"Royan","Farojan","Ojan","Dani"};
        int n;
        n = A.length;
        String x;
        System.out.println("Masukan Nama");
        x = in.next();
        PencarianString cari = new PencarianString();
        System.out.println("Hsil Pencarian Beruntun");
        System.out.println("Indeks Ke " + cari.SequentialSearch(A, n, x));
        System.out.println("Hasil Pencarian Bagi Dua");
        System.out.println("Indeks Ke " + cari.BinarySearch(A, n, x));
    }
}
