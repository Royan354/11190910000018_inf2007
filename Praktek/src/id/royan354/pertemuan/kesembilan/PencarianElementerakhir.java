/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class PencarianElementerakhir {
    public int getSearch(int L[], int n, int x) {
        int i = n - 1;
        boolean ketemu = false;
        while ((i > -1) && (i <= n - 1) && (!ketemu)) {
            if (i <= n - 1) {
                System.out.println("Posisi ke " + i + " Isi " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            }else {
                i = i - 1;
            }
        }
        if (ketemu) {
            return i;
        }else {
            return -1;
        }
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int L[] = {13, 16, 14, 21, 76, 15};
        int n, x;
        n = L.length;
        System.out.println("Masukan Angka Ynag Ingin Dicari");
        x = in.nextInt();
        PencarianElementerakhir seq = new PencarianElementerakhir();
        System.out.println(seq.getSearch(L, n, x));
    }
}
