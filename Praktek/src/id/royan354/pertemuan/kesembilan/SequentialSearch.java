package id.royan354.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class SequentialSearch {
    public boolean getSeqSearchBoolean(int L[], int x, int n)  {
        int i = 1;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke" + i + "Isi" + L[i]);
            }
            i = i + 1;
            System.out.println("Posisi Ke" + i + "Isi" + L[1]);
        }
        return L[i] == x;
    }
    public int getSeqSearchOutIndeks(int L[], int x, int n)  {
        int i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("Posisi ke" + i + "Isi" + L[i]);
            }
            i = i + 1;
            System.out.println("Posisi Ke" + i + "Isi" + L[1]);
        }
        if (L[i] == x) {
            return i;
        }else {
            return -1;
        }
    }
    public boolean getSeqSearchInBoolean(int L[], int x, int n)  {
        int i = 0;
        boolean ketemu = false;
        while ((i < n - 1) && (!ketemu)) {
            if (i == 0) {
                System.out.println("Posisi ke" + i + "Isi" + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            }else {
                i = i + 1;
                System.out.println("Posisi ke " + 1 + " Isi " + L[i]);
            }
        }return ketemu;
    }
    public int getSeqSearchInIndeks(int L[], int x, int n)  {
        int i = 0;
        boolean ketemu = false;
        while ((i < n - 1) && (!ketemu)) {
            if (i == 0) {
                System.out.println("Posisi ke" + i + "Isi" + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            }else {
                i = i + 1;
                System.out.println("Posisi ke " + 1 + " Isi " + L[i]);
            }
        }
        if (ketemu) {
            return i;
        }else {
            return -1;
        }
    }
    public int getSeqSearchSentinel(int L[], int x, int n)  {
        int i = 0;
        int idx;
        while (L[i] != x) {
            i++;
        }if (i == n + 1) {
            return idx = -1;
        }else {
            return idx = i;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int L[] = {13, 16, 14, 21, 76, 15};
        int n, x;
        n = L.length;
        x = in.nextInt();
        SequentialSearch seq = new SequentialSearch();
        System.out.println(seq.getSeqSearchBoolean(L, x, n));
    }
}