/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class JenisBilanganBulat {
    public static void main(String args[])  {
        Scanner in = new Scanner(System.in);
        int bilangan = in.nextInt();
        if (bilangan > 0) {
            System.out.println("Positif");
        } else if (bilangan < 0) {
            System.out.println("Negatif");
        } else if (bilangan == 0) {
            System.out.println("Nol");
        }
    }
}
