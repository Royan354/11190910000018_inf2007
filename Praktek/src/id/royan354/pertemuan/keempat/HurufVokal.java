/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class HurufVokal {
    public static void main(String args[])  {
        Scanner in = new Scanner(System.in);
        char k = in.next().charAt(0);
        if ((k == 'a') || (k == 'i') || (k == 'u') || (k == 'e') || (k == 'o')) {
            System.out.println("Huruf Vokal");
        } else {
            System.out.println("Huruf Konsonana");
        }
    }
}
