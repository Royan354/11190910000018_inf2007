/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class GanjilGenap {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int bilangan = in.nextInt();
        if (bilangan % 2 == 0)  {
            System.out.println("Bilangan Genap");
        } else {
            System.out.println("Bilangan Ganjil");
        }
    }
}