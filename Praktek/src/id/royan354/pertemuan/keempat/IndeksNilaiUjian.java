/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class IndeksNilaiUjian {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int nilai = in.nextInt();
        char indeks;
        
        if (nilai >= 80) {
            indeks = 'A';
        }else if ((nilai >= 70) && (nilai < 80)){
            indeks = 'B';
        }else if ((nilai >= 40) && (nilai < 70)) {
            indeks = 'C';
        }else {
            indeks = 'E';
        }        
        System.out.println(indeks);
    }
}
