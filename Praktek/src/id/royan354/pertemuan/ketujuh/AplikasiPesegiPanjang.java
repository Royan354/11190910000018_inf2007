/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class AplikasiPesegiPanjang {
    public static void main(String[] args) {
        PersegiPanjang persegiPanjang2 = new PersegiPanjang();
        
        Scanner in = new Scanner(System.in);
        double panjang, lebar;
        
        System.out.println("Masukan Panjang: ");
        panjang = in.nextDouble();
        
        System.out.println("Masukan Lebar: ");
        lebar = in.nextDouble();
        
        PersegiPanjang persegiPanjang = new PersegiPanjang(panjang, lebar);
        persegiPanjang.getinfo();
        System.out.println("Luas : " + persegiPanjang.getLuas());
        System.out.println("Keliling : " + persegiPanjang.getKeliling());
    }
}
