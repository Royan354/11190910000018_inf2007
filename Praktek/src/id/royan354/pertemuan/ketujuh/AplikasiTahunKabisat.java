package id.royan354.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class AplikasiTahunKabisat {
    public static void main(String[] args) {
        int Tahun;
        Kabisat kabisat = new Kabisat();
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan Tahun");
        Tahun = in.nextInt();
        if (kabisat.Kabisat(Tahun)) {
            System.out.println("Tahun Kabisat");
        }else {
            System.out.println("Bukan Tahun Kabisat");
        }
    }
}
