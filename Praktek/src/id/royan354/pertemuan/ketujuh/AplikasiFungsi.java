/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.ketujuh;

/**
 *
 * @author 62813
 */
public class AplikasiFungsi {
    public static void main(String[] args) {
        float x;
        Fungsi fungsi = new Fungsi();
        
        System.out.println("_______________");
        System.out.println("   x   f(x)    ");
        System.out.println("_______________");
        x = 10;
        while (x <= 15) {
            System.out.println(x + "        " + fungsi.f(x));
            x = x + (float) 0.2;
        }System.out.println("_______________");
    }
}
