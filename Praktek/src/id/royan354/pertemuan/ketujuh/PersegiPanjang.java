/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.ketujuh;

/**
 *
 * @author 62813
 */
public class PersegiPanjang {
    private double panjang;
    private double lebar;
    
    public PersegiPanjang() {
        System.out.println("Kontruktor Persegi Panjang");
    }
    public PersegiPanjang(double panjang, double lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }
    public double getLuas() {
        return panjang * lebar;
    }
    public double getKeliling() {
        return 2 * (panjang + lebar);
    }
    public void getinfo() {
        System.out.println("Kelas Persegi Panjang");
    }
}
