/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.royan354.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author 62813
 */
public class AplikasiSegitigaParameter {
    public static void main(String[] args) {
        int i, n;
        float a, t;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan Jumlah Buah Segitiga");
        n = in.nextInt();
        for (i = 1; i <= n; i++) {
            System.out.println("Masukan Nilai Alas");
            a = in.nextFloat();
            System.out.println("Masukan Nilai Tinggi");
            t = in.nextFloat();
            SegitigaParameter Segitiga = new SegitigaParameter(a, t);
            Segitiga.getLuas();
        }
    }
}
