package id.royan354.sepatu.model;

import java.time.LocalDateTime;
import java.math.BigDecimal;
/**
 *
 * @author 62813
 */
public class Sepatu {

    private static final long serialVersionUID = -6756463875294313469L;
    private int PilihNamaSepatu;
    private int PilihWarna;
    private int PilihUkuran;
    private String NamaPemesan;
    private String NoTelpon;
    private String Alamat;
    private int Jumlah;
    private BigDecimal Harga;
    private LocalDateTime waktuPemesanan;
    private boolean Keluar = false;

    public Sepatu() {

    }

    public Sepatu(int PilihNamaSepatu, int PilihWarna, int Ukuran, String NamaPemesan, String NoTelpon,
            String Alamat, int Jumlah, BigDecimal Harga, int Total, LocalDateTime Waktu) {
        this.Alamat = Alamat;
        this.Harga = Harga;
        this.Jumlah = Jumlah;
        this.NamaPemesan = NamaPemesan;
        this.PilihNamaSepatu = PilihNamaSepatu;
        this.NoTelpon = NoTelpon;
        this.waktuPemesanan = waktuPemesanan;
        this.PilihWarna = PilihWarna;
        this.PilihUkuran = PilihUkuran;
    }

    public int getPilihNamaSepatu() {
        return PilihNamaSepatu;
    }

    public void setPilihNamaSepatu(int PilihNamaSepatu) {
        this.PilihNamaSepatu = PilihNamaSepatu;
    }

    public int getNamaSepatu() {
        return PilihNamaSepatu;
    }

    public void setNamaSepatu(int NamaSepatu) {
        this.PilihNamaSepatu = NamaSepatu;
    }

    public int getPilihWarna() {
        return PilihWarna;
    }

    public void setPilihWarna(int PilihWarna) {
        this.PilihWarna = PilihWarna;
    }

    public int getPilihUkuran() {
        return PilihUkuran;
    }

    public void setPilihUkuran(int PilihUkuran) {
        this.PilihUkuran = PilihUkuran;
    }

    public String getNamaPemesan() {
        return NamaPemesan;
    }

    public void setNamaPemesan(String NamaPemesan) {
        this.NamaPemesan = NamaPemesan;
    }

    public String getNoTelpon() {
        return NoTelpon;
    }

    public void setNoTelpon(String NoTelpon) {
        this.NoTelpon = NoTelpon;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String Alamat) {
        this.Alamat = Alamat;
    }

    public int getJumlah() {
        return Jumlah;
    }

    public void setJumlah(int Jumlah) {
        this.Jumlah = Jumlah;
    }

    public BigDecimal getHarga() {
        return Harga;
    }

    public void setHarga(BigDecimal Harga) {
        this.Harga = Harga;
    }

    public LocalDateTime getWaktuPemesanan() {
        return waktuPemesanan;
    }

    public void setWaktuPemesanan(LocalDateTime waktuPemesanan) {
        this.waktuPemesanan = waktuPemesanan;
    }

    public boolean isKeluar() {
        return Keluar;
    }

    public void setKeluar(boolean Keluar) {
        this.Keluar = Keluar;
    }
    

    @Override
    public String toString() {
        return "Pesanan{" + "Nama Sepatu = " + PilihNamaSepatu + ", Warna = " + PilihWarna + ", Ukuran = " + PilihUkuran + ", Nama Pemesanan =" + NamaPemesan
                + ", Alamat = " + Alamat + ", Jumlah Barang = " + Jumlah + ", Harga = " + Harga + ", Waktu Pemesanan = " + waktuPemesanan + ", Keluar = " + Keluar +  '}';
    }
}
