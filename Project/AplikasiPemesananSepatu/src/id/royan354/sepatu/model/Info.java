package id.royan354.sepatu.model;

/**
 *
 * @author 62813
 */
public class Info {
    private final String aplikasi = "APLIKASI PEMESANAN SEPATU OJAN_STORE";
    private final String version = "Versi 1.0.0";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersion() {
        return version;
    }
}
