package id.royan354.sepatu.controller;

import id.royan354.sepatu.model.Sepatu;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import com.google.gson.Gson;

/**
 *
 * @author 62813
 */
public class SepatuController {

    private static final String FILE = "D:\\PemesananSepatu.json";
    private int PilihNamaSepatu;
    private Sepatu sepatu;
    private int PilihWarna;
    private int PilihUkuran;
    private String NamaPemesan;
    private String NoTelpon;
    private String Alamat;
    private int Jumlah;
    private BigDecimal Harga;
    private int Total;
    private LocalDateTime waktuPemesanan;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;
    private final Scanner in;
    private boolean Keluar = false;

    public SepatuController() {
        in = new Scanner(System.in);
        waktuPemesanan = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    public void setPesananSepatu() {
        Sepatu s = new Sepatu();
        System.out.println("===================================================");
        System.out.println("List Sepatu :");
        System.out.println("===================================================");
        System.out.println("1. Air Jordan 3 Retro		: Rp. 1.500.000");
        System.out.println("2. Air Jordan 1 High		: Rp. 5.000.000");
        System.out.println("3. Air Jordan 1 Low 		: Rp. 4.570.000");
        System.out.println("4. Air Jordan 5 Wings		: Rp. 2.000.000");
        System.out.println("5. Air Jordan 6 Travis Scott	: Rp. 3.400.000");
        System.out.print("Masukan Nama Sepatu : ");
        PilihNamaSepatu = in.nextInt();
        switch (PilihNamaSepatu) {
            case 1: {
                System.out.println("Harga : Rp.1.500.000");
            }
            break;
            case 2: {
                System.out.println("Harga : Rp.5.000.000");
            }
            break;
            case 3: {
                System.out.println("Harga : Rp.4.570.000");
            }
            break;
            case 4: {
                System.out.println("Harga : Rp.2.000.000");
            }
            break;
            case 5: {
                System.out.println("Harga : Rp.3.400.000");
            }
            break;
            default: {
                System.out.println("Tidak Ada List Sepatu Yang Anda Maksud Silakan Pilih (1-5)");
            }
        }
        System.out.println("======================");
        System.out.println("List Ukuran : ");
        System.out.println("======================");
        System.out.println("1. 6 UK	: 24 cm");
        System.out.println("2. 7 UK	: 25 cm");
        System.out.println("3. 8 UK	: 26 cm");
        System.out.println("4. 9 UK	: 27 cm");
        System.out.println("5. 10UK     : 28 cm");
        System.out.print("Masukan Pilihan Anda :");
        PilihUkuran = in.nextInt();
        switch (PilihUkuran) {
            case 1: {
                System.out.println("1. 6 UK	: 24 cm");
            }
            break;
            case 2: {
                System.out.println("2. 7 UK	: 25 cm");
            }
            break;
            case 3: {
                System.out.println("3. 8 UK	: 26 cm");
            }
            break;
            case 4: {
                System.out.println("4. 9 UK	: 27 cm");
            }
            break;
            case 5: {
                System.out.println("5. 10UK     : 28 cm");
            }
            break;
            default: {
                System.out.println("Ukuran Tidak Ada");
            }
        }
        System.out.println("======================");
        System.out.println("List Warna : ");
        System.out.println("======================");
        System.out.println("1. Red Black");
        System.out.println("2. Blue");
        System.out.println("3. White");
        System.out.println("4. Grey");
        System.out.println("5. Ocean Blue");
        System.out.print("Masukan Pilihan Anda :");
        PilihWarna = in.nextInt();
        switch (PilihWarna) {
            case 1: {
                System.out.println("1. Red Black");
            }
            break;
            case 2: {
                System.out.println("2. Blue");
            }
            break;
            case 3: {
                System.out.println("3. White");
            }
            break;
            case 4: {
                System.out.println("4. Grey");
            }
            break;
            case 5: {
                System.out.println("5. Ocean Blue");
            }
            break;
            default: {
                System.out.println("Tidak Ada Warna Yang Anda Maksud");
            }
        }

        System.out.println("======================");
        System.out.print("Masukkan Nama Anda : ");
        NamaPemesan = in.next();
        System.out.println("======================");

        System.out.print("Masukkan Alamat Anda : ");
        Alamat = in.next();
        System.out.println("======================");

        System.out.print("Masukkan Nomer Telpon Anda : ");
        NoTelpon = in.next();
        System.out.println("======================");

        System.out.print("Masukan Jumlah Pesanan : ");
        Jumlah = in.nextInt();

        String formatWaktuMasuk = waktuPemesanan.format(dateTimeFormat);
        System.out.println("Waktu Masuk : " + formatWaktuMasuk);

        sepatu = new Sepatu();
        sepatu.setNamaSepatu(PilihNamaSepatu);
        sepatu.setPilihWarna(PilihWarna);
        sepatu.setPilihUkuran(PilihUkuran);
        sepatu.setNamaPemesan(NamaPemesan);
        sepatu.setAlamat(Alamat);
        sepatu.setNoTelpon(NoTelpon);
        sepatu.setJumlah(Jumlah);
        sepatu.setHarga(Harga);
        sepatu.setWaktuPemesanan(waktuPemesanan);

        setWriteSepatu(FILE, sepatu);

        System.out.println("Apakah anda ingin input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setPesananSepatu();
        }
    }

    public void setKonfirmasiPembayaran() {
        System.out.println("Masukan Nama Pemesan : ");
        NamaPemesan = in.next();
        Sepatu s = getSearch(NamaPemesan);
        if (s != null) {
            System.out.println("Nama Pemesan\t= " + s.getNamaPemesan());
            System.out.println("No Telpon\t= " + s.getNoTelpon());
            System.out.println("Alamat\t= " + s.getAlamat());
            if (s.getNamaSepatu() == 1) {
                System.out.println("Nama Sepatu = Air Jordan 3 Retro");
            } else if (s.getNamaSepatu() == 2) {
                System.out.println("Nama Sepatu = Air Jordan 1 High");
            } else if (s.getNamaSepatu() == 3) {
                System.out.println("Nama Sepatu = Air Jordan 1 Low");
            } else if (s.getNamaSepatu() == 4) {
                System.out.println("Nama Sepatu = Air Jordan 5 Wings");
            } else if (s.getNamaSepatu() == 5) {
                System.out.println("Nama Sepatu = Air Jordan 6 Travis Scott");
            }

            if (s.getPilihWarna() == 1) {
                System.out.println("Warna = Red Black");
            } else if (s.getPilihWarna() == 2) {
                System.out.println("Warna = Blue");
            } else if (s.getPilihWarna() == 3) {
                System.out.println("Warna = White");
            } else if (s.getPilihWarna() == 4) {
                System.out.println("Warna = Grey");
            } else if (s.getPilihWarna() == 5) {
                System.out.println("Warna = Oceon Blue");
            }

            if (s.getPilihUkuran() == 1) {
                System.out.println("Ukuran = 6 UK: 24 cm");
            } else if (s.getPilihUkuran() == 2) {
                System.out.println("Ukuran = 7 UK: 25 cm");
            } else if (s.getPilihUkuran() == 3) {
                System.out.println("Ukuran = 8 UK: 26 cm");
            } else if (s.getPilihUkuran() == 4) {
                System.out.println("Ukuran = 9 UK: 27 cm");
            } else if (s.getPilihUkuran() == 5) {
                System.out.println("Ukuran = 10 UK: 28 cm");
            }
            if (s.getJumlah() > 0) {
                Harga = new BigDecimal(s.getJumlah());
                if (PilihNamaSepatu == 1) {
                    Harga = Harga.multiply(new BigDecimal(1500000));
                } else if (PilihNamaSepatu == 2) {
                    Harga = Harga.multiply(new BigDecimal(5000000));
                } else if (PilihNamaSepatu == 3) {
                    Harga = Harga.multiply(new BigDecimal(4570000));
                } else if (PilihNamaSepatu == 4) {
                    Harga = Harga.multiply(new BigDecimal(2000000));
                } else if (PilihNamaSepatu == 5) {
                    Harga = Harga.multiply(new BigDecimal(3400000));
                }
            }
            System.out.println("Harga : " + Harga);
            System.out.println("======================");
        }
        System.out.println("Proses Bayar?");
        System.out.print("1) Ya, 2) Tidak, 3) Keluar : ");
        pilihan = in.nextInt();
        switch (pilihan) {
            case 1:
                s.setKeluar(true);
                setWriteSepatu(FILE, s);
                break;
            case 2:
                setKonfirmasiPembayaran();
                break;
            default:
                Menu m = new Menu();
                m.getMenuAwal();
                break;
        }
        System.out.println("Apakah mau memproses kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setKonfirmasiPembayaran();
        }
    }

    public Sepatu getSearch(String NamaPemesan) {
        List<Sepatu> sepatus = getReadSepatu(FILE);// Arrays.asList(sepatu);

        Sepatu p = sepatus.stream()
                .filter(pp -> NamaPemesan.equalsIgnoreCase(pp.getNamaPemesan()))
                .findAny()
                .orElse(null);

        return p;
    }

    public List<Sepatu> getReadSepatu(String file) {
        List<Sepatu> sepatus = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Sepatu[] ps = gson.fromJson(line, Sepatu[].class);
                sepatus.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SepatuController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SepatuController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return sepatus;
    }

    public void setWriteSepatu(String file, Sepatu sepatu) {
        Gson gson = new Gson();

        List<Sepatu> sepatus = getReadSepatu(file);
        sepatus.remove(sepatu);
        sepatus.add(sepatu);

        String json = gson.toJson(sepatus);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(SepatuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setReviewSepatu() {
        File file = new File("D:\\ReviewSepatu.txt");
        try {
            Scanner scan = new Scanner(file);
            while (scan.hasNextLine()) {
                String getDataString = scan.nextLine();
                System.out.println(getDataString);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File Tidak Ditemukan");
        }
        Menu m = new Menu();
        m.getMenuAwal();
    }

    public void getDataSepatu() {
        List<Sepatu> sepatus = getReadSepatu(FILE);
        Predicate<Sepatu> isKeluar = e -> e.isKeluar() == true;
        Predicate<Sepatu> isDate = e -> e.getWaktuPemesanan().toLocalDate().equals(LocalDate.now());

        List<Sepatu> pResults = sepatus.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        BigDecimal Total = pResults.stream()
                .map(Sepatu::getHarga)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Nama Pemesan \tAlamat \t\tNo Telpon \t\tNama Sepatu \t\tWarna \t\tUkuran \t\tHarga \t\tWaktu Pemesanan");
        System.out.println("------------ \t------ \t\t--------- \t\t----------- \t\t----- \t\t------ \t\t----- \t\t----------------");
        pResults.forEach((p) -> {
            if (p.getNamaSepatu() == 1 && p.getPilihWarna() == 1 && p.getPilihUkuran() == 1) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 3 Retro" + "\t" + "Red Black" + "\t" + "6 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 2 && p.getPilihWarna() == 2 && p.getPilihUkuran() == 2) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 1 High" + "\t" + "Blue" + "\t\t" + "7 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 3 && p.getPilihWarna() == 3 && p.getPilihUkuran() == 3) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 1 Low" + "\t" + "White" + "\t" + "8 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 4 && p.getPilihWarna() == 4 && p.getPilihUkuran() == 4) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 5 Wings" + "\t" + "Grey" + "\t" + "9 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 5 && p.getPilihWarna() == 5 && p.getPilihUkuran() == 5) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 6 Travis Scott" + "\t" + "Grey" + "\t" + "9 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 2 && p.getPilihWarna() == 1 && p.getPilihUkuran() == 1) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 1 High" + "\t" + "Red Black" + "\t" + "6 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 3 && p.getPilihWarna() == 1 && p.getPilihUkuran() == 1) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 1 Low" + "\t" + "Red Black" + "\t" + "6 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 4 && p.getPilihWarna() == 1 && p.getPilihUkuran() == 1) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 5 Wings" + "\t" + "Red Black" + "\t" + "6 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 5 && p.getPilihWarna() == 1 && p.getPilihUkuran() == 1) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 6 Travis Scott" + "\t" + "Red Black" + "\t" + "6 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 1 && p.getPilihWarna() == 2 && p.getPilihUkuran() == 1) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 3 Retro" + "\t" + "Blue" + "\t" + "6 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 1 && p.getPilihWarna() == 3 && p.getPilihUkuran() == 1) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 3 Retro" + "\t" + "White" + "\t" + "6 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 1 && p.getPilihWarna() == 4 && p.getPilihUkuran() == 1) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 3 Retro" + "\t" + "Grey" + "\t" + "6 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 1 && p.getPilihWarna() == 5 && p.getPilihUkuran() == 1) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 3 Retro" + "\t" + "Ocean Blue" + "\t" + "6 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 1 && p.getPilihWarna() == 1 && p.getPilihUkuran() == 2) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 3 Retro" + "\t" + "Red Black" + "\t" + "7 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 1 && p.getPilihWarna() == 1 && p.getPilihUkuran() == 3) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 3 Retro" + "\t" + "Red Black" + "\t" + "8 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 1 && p.getPilihWarna() == 1 && p.getPilihUkuran() == 4) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 3 Retro" + "\t" + "Red Black" + "\t" + "9 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            } else if (p.getNamaSepatu() == 1 && p.getPilihWarna() == 1 && p.getPilihUkuran() == 5) {
                System.out.println(p.getNamaPemesan() + "\t\t" + p.getAlamat() + "\t\t" + p.getNoTelpon() + "\t\t\t" + "Air Jordan 3 Retro" + "\t" + "Red Black" + "\t" + "10 UK" + "\t\t" + p.getHarga()
                        + "\t\t" + p.getWaktuPemesanan().format(dateTimeFormat));
            }
        });
        System.out.println("------------ \t------ \t\t--------- \t\t----------- \t\t----- \t\t------ \t\t----- \t\t----------------");
        System.out.println("====================================");
        System.out.println("Total = Rp. " + Total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataSepatu();
        }
    }
}
