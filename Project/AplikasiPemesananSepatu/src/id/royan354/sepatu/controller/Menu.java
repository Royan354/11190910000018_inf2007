package id.royan354.sepatu.controller;

import java.util.Scanner;
import id.royan354.sepatu.model.Info;

/**
 *
 * @author 62813
 */
public class Menu {

    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");

        System.out.println("MENU UTAMA OJAN_STORE");
        System.out.println("1. Review Sepatu OJAN_STORE");
        System.out.println("2. Pemesanan Sepatu OJAN_STORE");
        System.out.println("3. Konfirmasi Pembayaran");
        System.out.println("4. Laporan Pemesanan");
        System.out.println("5. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3/4/5) : ");
        
        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3/4/5) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        SepatuController pc = new SepatuController();
        switch (noMenu) {
            case 1:
                pc.setReviewSepatu();
                break;
            case 2:
                pc.setPesananSepatu();
                break;
            case 3:
                pc.setKonfirmasiPembayaran();
                break;
            case 4: 
                pc.getDataSepatu();
            case 5:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;
        }
    }
}